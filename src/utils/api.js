import axios from "axios";
class API {
  axiosInstance = null;

  constructor() {
    /* 
      🚨1 point EXTRA CREDIT 🚨 👉🏿 get the baseURL from the environment
      https://create-react-app.dev/docs/adding-custom-environment-variables/
    */
    const axiosInstance = axios.create({
      baseURL: "http://kwitter-api.herokuapp.com/",
      timeout: 30000,
      headers: { Authorization: `Bearer ${getToken()}` }, //lookup getToken
    });

    axiosInstance.interceptors.request.use(
      (config) => ({
        ...config,
        headers: {
          ...config.headers,
          Authorization: `Bearer ${getToken()}`,
        },
      }),
      (error) => Promise.reject(error)
    );

    axiosInstance.interceptors.response.use(
      ({ data }) => data,
      (error) => Promise.reject(error)
    );

    this.axiosInstance = axiosInstance;
  }

  async getOneMessage(messageId) {
    try {
      const message = await this.axiosInstance.get("/messages/" + messageId);
      return message;
    } catch (err) {
      helpMeInstructor(err);
      throw err;
    }
  }

  async getMessages(username) {
    try {
      const result = await this.axiosInstance.get("/messages/", { username });
      console.log(result)
      return result;
    } catch (err) {
      helpMeInstructor(err);
      throw err;
    }
  }

  async postMessage(text) {
    try {
      const result = await this.axiosInstance.post("/messages", { text });
      return result;
    } catch (err) {
      helpMeInstructor(err);
      throw err;
    }
  }

  async addUser({ username, displayName, password }) {
    try {
      const result = await this.axiosInstance.post("/users",
        {
          username,
          displayName,
          password
        });
      return result;
    } catch (err) {
      helpMeInstructor(err);
      throw err;
    }
  }

  async updateUser({ password, about, displayName, username }) {
    try {
      console.log(username)
      const result = await this.axiosInstance.patch(`/users/${username}`,
        {
          about,
          displayName,
          password,
        });
      return result;
    } catch (err) {
      helpMeInstructor(err);
      throw err;
    }
  }

  async uploadPhoto(userName, photo) {
    try {
      const result = await this.axiosInstance.put(`/users/${userName}/picture`,
        photo,
        {
          headers: { "Content-Type": "multipart/form-data" }
        });
      console.log(result)
      return result;
    } catch (err) {
      helpMeInstructor(err);
      throw err;
    }
  }

  async getUser(username) {
    try {
      const result = await this.axiosInstance.get(`/users/${username}`,
        {
          username
        });
      return result;
    } catch (err) {
      helpMeInstructor(err);
      throw err;
    }
  }

  async postLikes({ messageId }) {
    try {
      const result = await this.axiosInstance.post("/likes", {
        messageId,
      });
      return result;
    } catch (err) {
      helpMeInstructor(err);
      throw err;
    }
  }

  async deleteLikes(likeId) {
    try {
      const result = await this.axiosInstance.delete(`/likes/${likeId}`)
      return result;
    } catch (err) {
      helpMeInstructor(err);
      throw err;
    }
  }

  async login({ username, password }) {
    try {
      const result = await this.axiosInstance.post("/auth/login", {
        username,
        password,
      });
      return result;
    } catch (err) {
      helpMeInstructor(err);
      throw err;
    }
  }

  async logout() {
    try {
      await this.axiosInstance.get("/auth/logout");
    } catch (err) {
      helpMeInstructor(err);
      throw err;
    }
  }
}

// WARNING.. do not touch below this line if you want to have a good day =]

function helpMeInstructor(err) {
  console.info(
    `
    Did you hit CORRECT the endpoint?
    Did you send the CORRECT data?
    Did you make the CORRECT kind of request [GET/POST/PATCH/DELETE]?
    Check the Kwitter docs 👉🏿 https://kwitter-api.herokuapp.com/docs/#/
    Check the Axios docs 👉🏿 https://github.com/axios/axios
    TODO: troll students
    `,
    err
  );
}

function getToken() {
  try {
    const storedState = JSON.parse(localStorage.getItem("persist:root"));
    return JSON.parse(storedState.auth).isAuthenticated; //parsing is turning string into javascript object
  } catch {
    return "";
  }
}

export default new API();