import React from 'react';
import { useDispatch } from 'react-redux';
import { LOGIN_SUCCESS } from '../../redux/actions';
import { Button } from "react-bootstrap";
import './GoogleLogin.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import lurker from './lurker.png'

export function GoogleLogin() {

    const dispatch = useDispatch() 

    const googleLoginForm = () => {
        const googleAuth = window.open("http://kwitter-api.herokuapp.com/auth/google/login")
        googleAuth.window.opener.onmessage = (event) =>{
            console.log(event)
            if (event.data.statusCode === 200) {
                dispatch({type: LOGIN_SUCCESS, payload: {username: event.data.username, token: event.data.token}})
                googleAuth.close()
            } else {
                googleAuth.close()
                return 
            }

        }
    }
    return (
        <div>
        <Button variant='outline-light' className="google-button" onClick={googleLoginForm}>Google SignIn</Button> {' '}
        <br></br>
        <img className="logo" src={lurker} width= "30%" height= "30%"/>
        </div>
    )
}