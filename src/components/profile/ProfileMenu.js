import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { messageActions } from "../../redux/actions/messages";
import { actions } from '../../redux/actions/auth';
import './ProfileMenu.css'

export const ProfileMenu = () => {
  const isAuthenticated = useSelector((state) => !!state.auth.isAuthenticated);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(messageActions.getMessages());
  }, []);

  const logout = () => dispatch(actions.logout());
  return (
    <div id="menu">
      <h1 id='profile-header'>LurkeR profilE</h1>
      <div id="menu-links">
        {isAuthenticated ? (
          <>
          <Link to="/lurkerfeed">Lurker Feed</Link>
            <Link to="/" onClick={logout}>
              Enough Lurking
            </Link>
          </>
        ) : null}
      </div>
    </div>
  );
};