import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { actions } from "../../redux/actions/auth";
import { Loader } from "../loader";
import { Link } from "react-router-dom";
import { addUser } from "../../redux/actions";
import { Button, Form, Col } from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import './LoginForm.css';

export const LoginForm = ({ login }) => {
  const { loading, error } = useSelector((state) => ({
    loading: state.auth.loading,
    error: state.auth.error,
  }));
  const dispatch = useDispatch(); // state for registration/sign up
  const INITIALSTATE = {
    username: "",
    displayName: "",
    password: "",
  }

  const addDefault = (defaultUser) => {
    dispatch(addUser(defaultUser))
  }

  const [state, setState] = useState({     // state for login 
    username: "",
    displayName: "",
    password: "",
  });

  const handleChange = (event) => {
    const inputName = event.target.name;
    const inputValue = event.target.value;
    setState((prevState) => ({ ...prevState, [inputName]: inputValue }));
  };

  const [registerState, setRegisterState] = useState({ ...INITIALSTATE })

  const handleLogin = (event) => {
    event.preventDefault();
    dispatch(actions.login(state));
  };

  const handleRegister = (event) => {
    console.log(registerState)
    console.log("work for god's sake")
    event.preventDefault();
    dispatch(addUser(registerState));
  };


  const handleRegChange = (event) => {
    const inputName = event.target.name;
    const inputValue = event.target.value;
    setRegisterState((prevState) => ({ ...prevState, [inputName]: inputValue }));
  };

  return (
    <React.Fragment>
      <div id="login-form">
        <div id="login">
          <div className="login-form">
            <Form onSubmit={handleLogin} class='text-center'>
              <Form.Group controlId="formBasicEmail" as={Col} xs={4}>
                <Form.Label htmlFor="username"
                  style={{
                    color: "whitesmoke",
                    fontSize: "16px"
                  }}>LurkerName</Form.Label>
                <Form.Control
                  type="text"
                  name="username"
                  value={state.username}
                  autoFocus
                  required
                  onChange={handleChange} />
                <Form.Text 
                className="text-muted"
                style={{
                  color: 'coral',
                  fontSize: "15px"
                }}
                >
                  Enter social security number here... seriously
                </Form.Text>

              </Form.Group>

              <Form.Group controlId="formBasicPassword" as={Col} xs={4}>
                <Form.Label>Password</Form.Label>
                <Form.Control
                  type="password"
                  name="password"
                  value={state.password}
                  required
                  onChange={handleChange} />
              </Form.Group>
              
                <Button variant='outline-light' className="Login-button" type="submit" disabled={loading}> {' '}
                Login
              </Button>
        
            </Form>
          </div>
          </div>
          <div id="logsub2">
            <Link to="/signup">Create Account</Link>
          </div>
      </div>
      {loading && <Loader />}
      {error && <p style={{ color: "red" }}>{error.message}</p>}
    </React.Fragment>
  );
};