import React from "react";
import { BrowserRouter, Switch } from "react-router-dom";
import { HomeScreen, ProfileScreen, NotFoundScreen, SignUpScreen, LurkerFeedScreen } from "../../screens";
import { ConnectedRoute } from "../connected-route/ConnectedRoute";


export const Navigation = () => (
  <BrowserRouter>

    <Switch>
      <ConnectedRoute
        exact
        path="/"
        redirectIfAuthenticated
        component={HomeScreen}
      />

      <ConnectedRoute
        exact
        isProtected
        path="/profiles/:username"
        component={ProfileScreen}
      />

      <ConnectedRoute
        exact
        path="/signup"
        redirectIfAuthenticated
        component={SignUpScreen}
      />

      <ConnectedRoute
        exact
        isProtected
        path="/lurkerfeed"
        component={LurkerFeedScreen}
      />

      <ConnectedRoute path="*" component={NotFoundScreen} />

    </Switch>
  </BrowserRouter>
);
