import React from "react";
import { Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { actions } from "../../redux/actions/auth";

export const SignUpFormMenu = () => {
  const isAuthenticated = useSelector((state) => !!state.auth.isAuthenticated);
  const dispatch = useDispatch();
  const logout = () => dispatch(actions.logout());
  return (
    <div id="menu">
      <h1>L.u.r.k.e.r..........account being created now... </h1>
      <div id="menu-links">
        {isAuthenticated ? (
          <>
            <Link to="/signup">Sign Up</Link>
            <Link to="/lurkerfeed">Lurker Feed</Link>
            <Link to="/" onClick={logout}>
              Enough Lurking
            </Link>
          </>
        ) : null}
      </div>
    </div>
  );
};



