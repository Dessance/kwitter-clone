import React from "react";
import { Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { actions } from "../../redux/actions/auth";
// import lurker from './lurker.png'
import "./Menu.css";

export const Menu = () => {
  const isAuthenticated = useSelector((state) => !!state.auth.isAuthenticated);
  const dispatch = useDispatch();
  const logout = () => dispatch(actions.logout());
  return (
    <div id="menu">
      <h1 className='home-header'>LurkeR</h1>
      <div id="menu-links">
        {isAuthenticated ? (
          <>
            <Link to="/lurkerfeed">Lurker Feed</Link>
            <Link to="/" onClick={logout}>
              Enough Lurking
            </Link>
          </>
        ) : null}
      </div>
    </div>
  );
};
