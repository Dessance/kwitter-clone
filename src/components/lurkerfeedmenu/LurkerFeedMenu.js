import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { messageActions } from "../../redux/actions/messages";
import { actions } from '../../redux/actions/auth';
import './LurkerFeedMenu.css'

export const LurkerFeedMenu = () => {
  const isAuthenticated = useSelector((state) => !!state.auth.isAuthenticated);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(messageActions.getMessages());
  }, []);

  const logout = () => dispatch(actions.logout());
  return (
    <div id="menu">
      <h1 id='feed-header'>LurkeR MessaGes</h1>
      <div id="menu-links">
        {isAuthenticated ? (
          <>
            <Link to="/profiles/:username">Profile</Link>
            <Link to="/" onClick={logout}>
              Enough Lurking
            </Link>
          </>
        ) : null}
      </div>
    </div>
  );
};