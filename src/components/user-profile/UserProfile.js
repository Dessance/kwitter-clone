import React, { createRef, useState, useEffect } from 'react';
import { useSelector, useDispatch } from "react-redux";
import { actions } from "../../redux/actions/user";
import './UserProfile.css'

const UserFunction = ({ addUser }) => {
  const fileUpload = createRef()

  const { user, username } = useSelector((state) => ({
    user: state.getUser.user,
    username: state.auth.username,
  }));

  useEffect(() => {
    dispatch(actions.getUserInfo(username))
    console.log(user.user.username)
  }, [])

  const [updateToggle, setUpdateToggle] = useState(false)

  const dispatch = useDispatch();

  const [state, setState] = useState({     // state for login 
    about: "",
    displayName: "",
    password: "",
    username: username,
  });

  const handleUpload = () => {
    console.log(fileUpload.current.files[0])
    const uploadedFile = fileUpload.current.files[0]
    const formData = new FormData()
    formData.append("picture", uploadedFile)
    dispatch(actions.uploadPhoto(username, formData))
    console.log(user)
  }

  const handleChange = (event) => {
    const inputName = event.target.name;
    const inputValue = event.target.value;
    setState((prevState) => ({ ...prevState, [inputName]: inputValue }));
  };

  const handleUpdateUser = (event) => {
    event.preventDefault();
    console.log(username)
    dispatch(actions.getUpdatedUserInfo(state));
  }

  return (
    <div className="App">
      <div className="profileIntro">Peek Through<div classname="userProfile"> <strong> {username}'s</strong> </div>  Door!</div>
      <div className="userData">
        {user.user &&
          <React.Fragment>
            <img src={`https://kwitter-api.herokuapp.com${user.user.pictureLocation}`} />
            <br></br>
            <br></br>
            <h2 className='username'>Lurker Name:
            <br>
              </br>{user.user.username}</h2>
            <br></br>
            <h5 id="about-text">About: <br></br>{user.user.about}</h5>
            <br></br>
            <h5 className="profile-display-name">Display Name: <br></br>{user.user.displayName}</h5>
            <br></br>
          </React.Fragment>}
      </div>
      <div className="uploadPhoto">
        <h4>Upload Photo here!</h4>
        <form>
          <button>
            <input type="file" onChange={handleUpload} ref={fileUpload}></input>
          </button>
        </form>
        <button onClick={() => setUpdateToggle(!updateToggle)}>Update Profile</button>
        {updateToggle &&
          <React.Fragment>
            <form id="login-form" onSubmit={handleUpdateUser}>
              <div>
                <h2 className="about"><label htmlFor="about">About</label></h2>
                <input
                  className="about"
                  type="text"
                  name="about"
                  value={state.about}
                  autoFocus
                  required
                  onChange={handleChange}
                />
              </div>
              <div>
                <h2 className="password"><label htmlFor="password">Password</label></h2>
                <input
                  className="password"
                  type="password"
                  name="password"
                  value={state.password}
                  required
                  onChange={handleChange}
                />
              </div>
              <div>
                <h2 className="display-name"><label htmlFor="displayName">displayName</label></h2>
                <input
                  className="display-name"
                  type="displayName"
                  name="displayName"
                  value={state.displayName}
                  required
                  onChange={handleChange}
                />
              </div>
              <button className='update-user' type="submit"> Let's see! </button>
            </form>
          </React.Fragment>
        }
      </div>
    </div>
  )
}
export default UserFunction