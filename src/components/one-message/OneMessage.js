import React from 'react';
import { Button } from 'antd';
import { useSelector, useDispatch } from "react-redux";
import { actions } from "../../redux/actions/one-message";

const OneMessageTestButton = ({ getOneMessage }) => {
  const { message } = useSelector((state) => state.messages)

  const dispatch = useDispatch();

  const handleOneMessage = () => {
    dispatch(actions.getOneMessage(message));
  };
  const oneMessage = message.map(eachMessage => (eachMessage.text))
  console.log(oneMessage)

  return (
    <div className="App">
      <Button type="primary" onClick={handleOneMessage}>Messages</Button>
      <ul>
        {[oneMessage]}
      </ul>
    </div>
  )
};
export default OneMessageTestButton