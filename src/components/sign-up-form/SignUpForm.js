import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Loader } from "../loader";
import { addUser } from "../../redux/actions/user"


export const SignUpForm = ({ login }) => {
  const { loading, error } = useSelector((state) => ({
    loading: state.auth.loading,
    error: state.auth.error,
  }));


  const dispatch = useDispatch();

  const [state, setState] = useState({
    username: "",
    displayName: "",
    password: "",
  });

  const handleLogin = (event) => {
    event.preventDefault();
    dispatch(addUser(state));
  };

  const handleChange = (event) => {
    const inputName = event.target.name;
    const inputValue = event.target.value;
    setState((prevState) => ({ ...prevState, [inputName]: inputValue }));
  };

  const addDefault = (defaultUser) => {
    dispatch(addUser(defaultUser))
  }

  return (
    <React.Fragment>

      <form id="signup-form" onSubmit={addDefault}>

        <label htmlFor="username">Username</label>

        <input
          type="text"
          name="username"
          value={state.username}
          autoComplete="username"
          autoFocus
          required
          onChange={handleChange}
        />

        <label htmlFor="displayName">displayName</label>

        <input
          type="text"
          name="displayName"
          value={state.displayName}
          autoComplete="name"
          required
          onChange={handleChange}
        />

        <label htmlFor="password">Password</label>

        <input
          type="password"
          name="password"
          value={state.password}
          autoComplete="current-password"
          required
          onChange={handleChange}
        />

        <button type="submit" disabled={loading} onClick={handleLogin}>
          Create Lurker Account
        </button>

      </form>

      {loading && <Loader />}
      {error && <p style={{ color: "red" }}>{error.message}</p>}
    </React.Fragment>
  );
};