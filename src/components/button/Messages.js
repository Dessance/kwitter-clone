import React, { useEffect, useState } from "react";
import { Button, ListGroup, Card } from "react-bootstrap";
import { useSelector, useDispatch } from "react-redux";
import { actions } from "../../redux/actions/likes";
import { messageActions } from "../../redux/actions/messages";
import 'bootstrap/dist/css/bootstrap.min.css';
import './Messages.css';

const Messages = ({ getMessages }) => {
  const { messages, count, username } = useSelector((state) => ({
    messages: state.messages.messages,
    username: state.auth.username
  }));

  const dispatch = useDispatch();

  const handleLikes = (message) => {
    let ifUserFound = false
    message.likes.map((like) => {
      if (like.username === username) {
        dispatch(actions.deleteLikes(like.id))
        ifUserFound = true
      }
    })
    if (ifUserFound === false) {
      dispatch(actions.postLikes(message.id));
      console.log(messages.likes);
    }
  };


  const [text, setText] = useState('');

  useEffect(() => {
    dispatch(messageActions.getMessages());
    console.log(messages)
  }, []);

  const handleChange = (event) => {
    const textValue = event.target.value;
    setText(textValue);
  };

  const handleMessages = () => {
    dispatch(messageActions.postMessage(text));
  };

  return (
    <div className="App">
      <input
        type="text"
        name="text"
        value={text}
        required
        onChange={handleChange}
      />
      <Button className='create-message-button' variant="outline-light" onClick={handleMessages}>Create Message</Button> {' '}
      <ul>{messages && messages.map(eachMessage => (
        <Card style={{ width: '18rem'}}>
        <ListGroup variant="flush">
          <ListGroup.Item>{eachMessage.text}</ListGroup.Item>
          <Button className="like" variant="outline-light" onClick={() => handleLikes(eachMessage)}>Like</Button>{eachMessage.likes.length} Likes
        </ListGroup>
      </Card>
      ))}</ul>
    </div>
  )
};
export default Messages
