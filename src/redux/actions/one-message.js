import api from '../../utils/api';
export const MESSAGE_LOADED = "MESSAGE_LOADED"
export const FAILURE = "MESSAGE_FAILURE"
export const LOAD = "MESSAGE_LOAD"
export const DELETE = "MESSAGE_DELETE"

const getOneMessage = () => async (dispatch, getState) => {
  try {
    dispatch({ type: LOAD });
    const payload = await api.getOneMessage();
    console.log({ payload })
    dispatch({ type: MESSAGE_LOADED, payload });
  } catch (err) {
    dispatch({
      type: FAILURE,
      payload: err.message,
    });
  }
};

export const actions = {
  getOneMessage,
};
