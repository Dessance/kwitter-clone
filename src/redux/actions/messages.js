import api from "../../utils/api";
export const POST_MESSAGE = "POST_MESSAGE";
export const LIKES_LOADED = "LIKES_LOADED";
export const MESSAGES_LOADED = "MESSAGES_LOADED";
export const FAILURE = "MESSAGES_FAILURE";
export const LOAD = "MESSAGES_LOAD";
export const DELETE_MESSAGE = "DELETE_MESSAGE";

const getMessages = () => async (dispatch, getState) => {
  try {
    dispatch({ type: LOAD });
    const payload = await api.getMessages();
    console.log(payload);
    dispatch({ type: MESSAGES_LOADED, payload });
  } catch (err) {
    dispatch({
      type: FAILURE,
      payload: err.message,
    });
  }
};

const postMessage = (text) => async (dispatch, getState) => {
  try {
    dispatch({ type: LOAD });
    const payload = await api.postMessage(text);
    console.log({ payload })
    dispatch({ type: POST_MESSAGE, payload });
    dispatch(getMessages())
  } catch (err) {
    dispatch({
      type: FAILURE,
      payload: err.message,
    });
  }
};

export const messageActions = {
  getMessages,
  postMessage,
};