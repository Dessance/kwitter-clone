import api from "../../utils/api";
export const LIKES_LOADED = "LIKES_LOADED";
export const MESSAGES_LOADED = "MESSAGES_LOADED";
export const POST_LIKES = "POST_LIKES";
export const DELETE_LIKES = "DELETE_LIKES";
export const FAILURE = "LIKES_FAILURE";
export const LOAD = "LIKES_LOAD";

const postLikes = (messageId) => async (dispatch, getState) => {
  try {
    dispatch({ type: LOAD });
    await api.postLikes({ messageId });
    const payload = await api.getMessages();
    console.log({ payload });
    dispatch({ type: MESSAGES_LOADED, payload });
  } catch (err) {
    dispatch({
      type: FAILURE,
      payload: err.message,
    });
  }
};

const deleteLikes = (likeId) => async (dispatch, getState) => {
  try {
    dispatch({ type: LOAD });
    await api.deleteLikes(likeId);
    const payload = await api.getMessages();
    console.log({ payload });
    dispatch({ type: MESSAGES_LOADED, payload });
  } catch (err) {
    dispatch({
      type: FAILURE,
      payload: err.message,
    });
  }
};

export const actions = {
  postLikes,
  deleteLikes,
};
