import api from '../../utils/api'
export const GET_USER = "GET_USER"
export const ADD_USER = "ADD_USER"
export const FAILURE = "FAILURE"
export const LOAD = "LOAD"

export const addUser = (credentials) => async (dispatch, getState) => {
  try {
    dispatch({ type: LOAD });
    const payload = await api.addUser(credentials);
    dispatch({ type: ADD_USER, payload });
    console.log({ payload })
  } catch (err) {
    dispatch({
      type: FAILURE,
      payload: err.message,
    });
  }
}

const getUserInfo = (username) => async (dispatch, getState) => {
  try {
    dispatch({ type: LOAD });
    const payload = await api.getUser(username);
    console.log(payload)
    dispatch({ type: GET_USER, payload });
  } catch (err) {
    dispatch({
      type: FAILURE,
      payload: err.message,
    });
  }
}
const getUpdatedUserInfo = (updatedInfo) => async (dispatch, getState) => {
  try {
    console.log(getState())
    dispatch({ type: LOAD });
    await api.updateUser(updatedInfo);
    const payload = await api.getUser(getState().auth.username);
    console.log(payload)
    dispatch({ type: GET_USER, payload });
  } catch (err) {
    dispatch({
      type: FAILURE,
      payload: err.message,
    });
  }
}

const uploadPhoto = (userName, photo) => async (dispatch, getState) => {
  try {
    dispatch({ type: LOAD });
    await api.uploadPhoto(userName, photo);
    const payload = await api.getUser(userName);
    console.log(payload)
    dispatch({ type: GET_USER, payload });
  } catch (err) {
    dispatch({
      type: FAILURE,
      payload: err.message,
    });
  }
}

export const actions = {
  getUserInfo,
  getUpdatedUserInfo,
  uploadPhoto,
}