import { combineReducers } from "redux";
import { authReducer } from "./auth";
import  messagesReducer  from "./messages"
import { getUserReducer } from "./getUsers"
import  oneMessageReducer from "./one-message";
export default combineReducers(
    { auth: authReducer, 
        // users: defaultUserReducer, 
        messages: messagesReducer,
        getUser: getUserReducer,
        message: oneMessageReducer
    });
