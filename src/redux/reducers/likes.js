import { LIKES_LOADED, DELETED_LIKES, FAILURE, LOAD } from '../actions'

const INITIAL_STATE = {}

const likesReducer = (state = { ...INITIAL_STATE }, action) => {
    switch (action.type) {
        case LOAD:
            return {
                ...INITIAL_STATE,
                loading: true,
            };

        case LIKES_LOADED:
            const { likes } = action.payload;
            return {
                ...INITIAL_STATE,
                likes,
                loading: false,
            };

        case DELETED_LIKES:
            const { likes } = action.payload;
            return {
                ...INITIAL_STATE,
                likes,
                loading: false,
            };

        case FAILURE:
            return {
                ...INITIAL_STATE,
                error: action.payload,
                loading: false,
            };
        default:
            return state;
    }
}

export default likesReducer