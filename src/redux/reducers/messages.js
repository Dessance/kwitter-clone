// TODO: implement
import {
  MESSAGES_LOADED,
  POST_MESSAGE,
  DELETE_MESSAGE,
  FAILURE,
  LOAD
} from "../actions";

const INITIAL_STATE = {
  loading: false,
  error: "",
  messages: [],
  count: 0,
};

const messagesReducer = (state = { ...INITIAL_STATE }, action) => {
  switch (action.type) {
    case LOAD:
      return {
        ...INITIAL_STATE,
        loading: true,
      };

    case MESSAGES_LOADED:
      const { messages, count } = action.payload;
      return {
        ...INITIAL_STATE,
        messages,
        count,
        loading: false,
      };

    case POST_MESSAGE:
      const { message, counts } = action.payload;
      return {};

    case DELETE_MESSAGE:
      return {};

    case FAILURE:
      return {
        ...INITIAL_STATE,
        error: action.payload,
        loading: false,
      };
    default:
      return state;
  }
};

export default messagesReducer;
