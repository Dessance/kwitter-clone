import { MESSAGE_LOADED, FAILURE, LOAD, DELETE } from '../actions'

const INITIAL_STATE = {
    loading: false,
    error: "",
    id: 0,
    text: "",
    username: "",
    createdAt: "",
    likes: []
}

const oneMessageReducer = (state = { ...INITIAL_STATE }, action) => {
    switch (action.type) {
        case LOAD:
            return {
                ...INITIAL_STATE,
                loading: true,
            };

        case MESSAGE_LOADED:
            const { id, text, username, createdAt, likes } = action.payload;
            return {
                ...INITIAL_STATE,
                id,
                text,
                username,
                createdAt,
                likes,
                loading: false,
            };

        case FAILURE:
            return {
                ...INITIAL_STATE,
                error: action.payload,
                loading: false,
            };

        case DELETE:
            return {
                id: 0,
                text: "",
                username: "",
                createdAt: "",
                likes: []
            }
        default:
            return state;
    }
}

export default oneMessageReducer