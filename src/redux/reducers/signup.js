import { SIGN_UP, SIGN_UP_SUCCESS, SIGN_UP_FAILURE } from "../actions";

const INITIAL_STATE = {
  username: "",
  displayName: "",
  password: "",
  loading: false,
  error: "",
};

export const signUpReducer = (state = { ...INITIAL_STATE }, action) => {
  switch (action.type) {
    case SIGN_UP:
      return {
        ...INITIAL_STATE,
        loading: true,
      };

    case SIGN_UP_SUCCESS:
      const { username, displayName, password } = action.payload;
      return {
        ...INITIAL_STATE,
        displayName,
        username,
        password,
        loading: false,
      };

    case SIGN_UP_FAILURE:
      return {
        ...INITIAL_STATE,
        error: action.payload,
        loading: false,
      };

    default:
      return state;
  }
};
