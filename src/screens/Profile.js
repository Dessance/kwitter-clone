import React from "react";
import UserFunction from "../components/user-profile/UserProfile";
import { ProfileMenuContainer } from "../components/profile";
import './Profile.css'

export const ProfileScreen = () => (
  <>
    <ProfileMenuContainer />
   <br></br>
    <h2 className="welcome">Welcome </h2>
    <UserFunction className="user-function"></UserFunction>
    {/* <Messages></Messages> */}
    {/* <h4>Lurker Level: 0 (state code this)</h4> */}
  </>
);