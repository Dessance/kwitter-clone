import React from "react";
import { LoginFormContainer, MenuContainer, GoogleLogin } from "../components";

export const HomeScreen = () => (
  <>
    <MenuContainer />
    <LoginFormContainer />
    <GoogleLogin/>
  </>
);