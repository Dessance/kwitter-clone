import React from "react";
import { SignUpFormContainer, MenuContainer} from "../components/";
import { Link } from "react-router-dom";

export const SignUpScreen = () => (
  <>
    <MenuContainer />
    <h2>Create New Lurker Below:</h2>
    <SignUpFormContainer />

    <Link to="/">Go Home</Link>
  </>
);