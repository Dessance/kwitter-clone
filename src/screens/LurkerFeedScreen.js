import React from "react";
import { LurkerFeedMenuContainer } from "../components/lurkerfeedmenu";
import Messages from "../components/button/Messages";
import './LurkerFeedScreen.css'

export const LurkerFeedScreen = () => (
  <>
    <LurkerFeedMenuContainer/>
    <h4 className="dms">Direct Messages
      <br></br>
      (Slide in Here)
    </h4>
    <Messages/>
  </>
);